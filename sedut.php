<?php
/**
* Plugin Name: Sedut Maukerja
* Plugin URI: https://www.facebook.com/shahruld1
* Description: Private plugin system to sedut maukerja content
* Version: 1.0
* Author: Shah Danial
* Author URI: https://www.facebook.com/shahruld1
**/

# CONSTRUCT
include('helper/helper.php'); // common helper..
include('core/activeDeactive.php'); // once plugin activated or deactivated
include('helper/getDbData.php'); // get data from to then serve in plugin page forms
include('helper/adminPage.php'); // plugin page froms
include('helper/formAction.php'); // once submit form from plugin page option
