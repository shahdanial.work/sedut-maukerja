<?php

function setContent($api, $data)
{
    $rotator = getLowRoator();

    $newLine = PHP_EOL;
    $contentHTML = '';

    # perenggan 1
    if(isset($rotator[0])){
      if(isset($rotator[0]['perenggan'])){
        $contentHTML .= str_replace('$title', $data['wp_fields']['post_title'], $rotator[0]['perenggan']);
      }
    }

    # requirement
    if(isset($api['requirement'])){
      $para = formatPerenggan($api['requirement']);
      if(strlen($para) > 0){
        $contentHTML .= '<!-- hook_before_requirementBox --><div class="requirements">';
        $contentHTML .= $newLine . $newLine . '<p class="heading">Kelayakan dan penerangan</p><!-- hook_after_requirementHeading -->';
        $contentHTML .= $newLine . $para;
        $contentHTML .= '</div><!-- hook_after_requirementBox -->';
      }
    }

    # benefit
    $benefit = '';
    if(isset($api['benefit_ms'])){
      $para = formatPerenggan($api['benefit_ms']);
      if(strlen($para) > 0) {
        $benefit .= $para;
      }
    }
    if (isset($api['benefit_en'])){
      $para1 = formatPerenggan($api['benefit_en']);
      if(strlen($para1) > 0) {
        $benefit .= $newLine . $para1;
      }
    }
    if (isset($api['benefit_zh'])){
      $para2 = formatPerenggan($api['benefit_zh']);
      if(strlen($para2) > 0) {
        $benefit .= $newLine . $para2;
      }
    }

    if(strlen(trim($benefit)) > 0) {
      $contentHTML .= '<div class="benefits">';
      $contentHTML .= $newLine . $newLine . '<p class="heading">Benefit ditawarkan</p><!-- hook_after_benefitHeading -->';
      $contentHTML .= $newLine . formatPerenggan($benefit);
      $contentHTML .= '</div>';
      $contentHTML .= '<!-- hook_after_benefitBox -->';
    }

    # perenggan 1.1
    if(isset($rotator[1])){
      if(isset($rotator[1]['perenggan'])){
        $contentHTML .= $newLine . $newLine . str_replace('$title', $data['wp_fields']['post_title'], $rotator[1]['perenggan']);
      }
    }

    // echo apply_filters('the_content', $contentHTML);

    $data['wp_fields']['post_content'] = $contentHTML;

    // echo '<h1>API</h1>';
    // echo '<textarea style=" width: 100%; height: 300px; ">';
    // print_r($api);
    // echo '</textarea>';
    // echo '<h1>Rotator</h1>';
    // echo '<textarea style=" width: 100%; height: 300px; ">';
    // print_r($rotator);
    // echo '</textarea>';
    // echo '<h1>Data</h1>';
    // echo '<textarea style=" width: 100%; height: 300px; ">';
    // print_r($data);
    // echo '</textarea>';
    // echo '<br><hr><br>';

    publishPost($api, $data);
    recordRotatorUsed($rotator);
}
