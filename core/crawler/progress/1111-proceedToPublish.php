<?php

/**
 * this function will process to publish the post in API if meet requirements. If All posts succeed it will return (bool) true. When ever there's post already publised or facing error, it will return (bool) false. So you can call this function inside while-true loop.
 */
function proceedToPublish($apis)
{
    // echo '<h1>API</h1>';
    // echo '<textarea style=" width: 100%; height: 300px; ">';
    // print_r($apis);
    // echo '</textarea><br>';
    /*
    this loop is filter:
    if not meet requirements, unset them from object..
    if combination source AND source_post_id already in DB, return ASAP = stop crawling older posts..
    */
    foreach ($apis as $key => $api) {
        $pass = array(); // if there's boolean false on this array, current item ($api), will remove/unset from object

        # check if  already posted by using method, source_post_id and source_domain..
        if(!isThisUniquePost($api, 'maukerja.com')){
            // stop this loop processor when current items found post that already posted. Older API posts must be already posted too!
            array_push($pass, false);
        }

        # Must have title..
        $title = '';
        if (isset($api['title'])) {
            $title = trim($api['title']);
        }
        if (strlen($title) < 1 && isset($api['job_title'])) {
            $titlePass = trim($api['job_title']);
        }
        if (strlen($title) < 1 ) {
            array_push($pass, false);
        }
        /*
        no need to stop posting whenever there's already same title. this fields usually use similar title circle. We already made algo for dynamic title and no duplicate!
        else {
            # double check.. title must be unique
            if(post_exists($title,$content = '',$date = '', $type = 'post' )) array_push($pass, false);
        }
        */

        # Must have content
        $contentPass = '';
        if (isset($api['requirement'])) {
            $contentPass .= trim($api['requirement']);
        }
        if (isset($api['benefit_en'])) {
            $contentPass .= trim($api['benefit_en']);
        }
        if (isset($api['benefit_ms'])) {
            $contentPass .= trim($api['benefit_ms']);
        }
        if (strlen($contentPass) === 0) {
            array_push($pass, false);
        }

        if(in_array(false, $pass)) unset($apis[$key]);

    }
    // print_r('111111');
    # proceed loop..
    try {
        foreach ($apis as $api) {
            buildObjKey($api);
        }
        return true;
    } catch (Exception $e) {
        echo $e->getMessage();
        return false;
    }
    catch (InvalidArgumentException $e) {
        echo $e->getMessage();
        return false;
    }

    // everything smooth? return true, so previous processor who's called this function can proceed crawling next page by change pagenition number to next page number
    return true;

}
