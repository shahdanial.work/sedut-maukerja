<?php

/**
 * api must be ada array from API
 */
function buildObjKey($apis)
{

    $data = array(
      #default post required key's for db purpose
    'wp_fields' => array(
      'post_title' => '',
      'post_content' => '',
      'post_status' => 'publish',
      'post_date' => current_time('mysql'),
      'post_author' => 1,
      'post_type' => 'post',
      'post_category' => array(1) // default, 1 = Uncategorize  .. overide them in next stage function.
    ),
      #custom fields
    'custom_fields' => array(
      'source_domain' => 'maukerja.com',
      'source_post_id' => null,
      'region' => null,
      'job_locations' => null,
      'job_type' => null,
      'job_branch' => null,
      'salary' => null,
      'speaking_language' => null,
      'contact_phone' => null,
      'contact_email' => null,
      'apply_link' => null,
      'expiry_date' => null,
      'company_name' => null,
      'company_logo' => null
    )
  );
  // echo '222222';
  apiToData($apis, $data);

}
