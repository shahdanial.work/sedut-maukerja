<?php

function setTitle($api, $data)
{
//   print_r('hi');
    if (isset($api['title'])) {
        if (strlen(trim($api['title'])) > 0) {
            $data['wp_fields']['post_title'] = dynamicWpPostTitle($api['title'], $data['custom_fields']['company_name']);
        }
    }
    if (!$data['wp_fields']['post_title'] && isset($api['job_title'])) {
        if (strlen(trim($api['job_title'])) > 0) {
            $data['wp_fields']['post_title'] = dynamicWpPostTitle($api['job_title'], $data['custom_fields']['company_name']);
        }
    }

    # title doesn't exists? Oh snap, title is 1 of important thing..
    if (!$data['wp_fields']['post_title']) {
        return;
    }
    setContent($api, $data);
}
