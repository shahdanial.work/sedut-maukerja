<?php

function apiToData($api, $data)
{

    # CUSTOM FIELDS
    if (isset($data['custom_fields'])) {
        if (isset($api['job_id'])) {
            if(strlen(trim($api['job_id'])) > 0)
            $data['custom_fields']['source_post_id'] = trim($api['job_id']);
        }
        if(isset($api['region'])) {
            $data['custom_fields']['region'] = strArrDbFormatter($api['region']);
        }
        if(isset($api['job_locations'])) {
            $data['custom_fields']['job_locations'] = strArrDbFormatter($api['job_locations']);
        }
        if(isset($api['job_type'])) {
            $data['custom_fields']['job_type'] = strArrDbFormatter($api['job_type']);
        }
        if(isset($api['job_branch'])) {
            $data['custom_fields']['job_branch'] = strArrDbFormatter($api['job_branch']);
        }
        // $data['salary'] = $api['final_salary']; // maukerja use `Log masuk untuk melihat gaji` to all posts, so always true
        if(isset($api['speaking_language'])) {
            $data['custom_fields']['speaking_language'] = strArrDbFormatter($api['speaking_language']);
        }
        if(isset($api['contact_phone'])) {
            $data['custom_fields']['contact_phone'] = strArrDbFormatter($api['contact_phone']);
        }
        if(isset($api['contact_email'])) {
            $data['custom_fields']['contact_email'] = strArrDbFormatter($api['contact_email']);
        }
        if(isset($api['expiry_date'])) {
            if(strlen(trim($api['expiry_date'])) > 0)
            $data['custom_fields']['expiry_date'] = trim($api['expiry_date']);
        }
        if(isset($api['apply_link'])) {
            if(strlen(trim($api['apply_link'])) > 0)
            $data['custom_fields']['apply_link'] = trim($api['apply_link']);
        }

        # apply link still null?
        if(
            !$data['custom_fields']['apply_link']
            &&
            isset($data['custom_fields']['source_post_id'])
        ){
            $data['custom_fields']['apply_link'] = 'https://www.maukerja.my/job/' . $data['custom_fields']['source_post_id'] . '-kerja';
        }
    }

    # Setup Company
    if(isset($api['company_name'])) $data['custom_fields']['company_name'] = $api['company_name'];
    # logo company
    if($data['custom_fields']['company_name'] && isset($api['logo'])){
        $sourceImg = $api['logo'];
        if(strlen(trim($sourceImg)) > 4){
            // print_r('lepas<br>');
            $tryUpload = uploadImageByUrl($sourceImg, $data['custom_fields']['company_name']);
            // var_dump($tryUpload);
            if($tryUpload){
                // print_r('lepas 2<br>');
                if(isset($tryUpload['error']) && isset($tryUpload['url'])){
                    if(!$tryUpload['error'] && strlen(trim($tryUpload['url'])) > 4){
                        $data['custom_fields']['company_logo'] = trim($tryUpload['url']);
                    }
                }
            }
        }
    }
    // echo 'LLL';
    setCategory($api, $data);

}
