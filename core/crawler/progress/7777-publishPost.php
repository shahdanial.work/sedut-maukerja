<?php

function publishPost($api, $data) {
  # insert new post using variable that return new post id
  $newpost_id = wp_insert_post($data['wp_fields']);
  # add post meta by post id
  if(is_array($data['custom_fields'])){
    foreach ($data['custom_fields'] as $key => $value) {
      add_post_meta($newpost_id, $key, $value);
    }
  }
}