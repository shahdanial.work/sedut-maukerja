<?php

function setCategory($api, $data)
{

  # Algorthm before create wordpress category
    if (isset($api['category_name'])) {
        if (strlen(trim($api['category_name'])) > 3) {
            # API ada category ..
            $api['category_name'] = trim($api['category_name']);
            # Maukerja category by title..
            if(strpos($api['category_name'], ' ') !== false || ctype_alpha($api['category_name'])) {
              if ( ! function_exists( 'category_exists' ) ) {
                require_once( ABSPATH . 'wp-admin/includes/taxonomy.php' );
              }
              $categoryExists = category_exists($api['category_name']);
              if(!$categoryExists) {
                $tryInsertNewCat = wp_create_category($api['category_name']);
                if(is_numeric($tryInsertNewCat)) $data['wp_fields']['post_category'] = array($tryInsertNewCat);
              } else {
                if(is_numeric($categoryExists)) $data['wp_fields']['post_category'] = array($categoryExists);
              }
            }
        }
    }
    setTitle($api, $data);

}
