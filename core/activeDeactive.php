<?php

# hook `deactivated_plugin` will execute `sedutDeactivated` once plugin deactivated in wp-admin
add_action('deactivated_plugin', 'sedutDeactivated');
function sedutDeactivated()
{
    wp_clear_scheduled_hook('sedutPerHour');
}


# Once click active the plugin in plugins page
add_action('activated_plugin', 'sedutActivated');
function sedutActivated()
{
    # register `sedutPerHour` cron job if this cron job not exists or run
    if (! wp_next_scheduled('sedutPerHour')) {
        wp_schedule_event(time(), 'hourly', 'sedutPerHour'); // hourly
    }
}

# registered cron job with name `sedutPerHour` execute function sedutMauKerja function
add_action('sedutPerHour', 'sedutMaukerja');
function sedutMaukerja()
{
    include('crawler/crawler.php');
    startCrawl();
}
