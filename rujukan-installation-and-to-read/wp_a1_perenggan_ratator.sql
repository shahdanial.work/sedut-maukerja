-- You need to run this SQL query manually in your Mysql Database 1 by 1 for installation

-- 1
CREATE TABLE wp_a1_perenggan_ratator (
  `id` int(11) NOT NULL,
  `pNumber` varchar(20) CHARACTER SET utf8 NOT NULL,
  `perenggan` longtext CHARACTER SET utf8,
  `used` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 2
ALTER TABLE `wp_a1_perenggan_ratator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pNumber` (`pNumber`);

-- 3
ALTER TABLE `wp_a1_perenggan_ratator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

-- 4
INSERT INTO `wp_a1_perenggan_ratator` (`id`, `pNumber`, `perenggan`, `used`) VALUES
(1, '1', 'This is perenggan 1 AA BB CC', 1),
(2, '1_1', 'This is para 1.1 CCCCCCCC', 1),
(3, '2', 'this is para 2 DDDDDDD', 2),
(4, '2_1', 'Ini para 2.1 EEEEEEEEEEEEE', 2),
(5, '3', 'this is para 3', 3),
(6, '3_1', 'para 3.1', 3),
(7, '4', 'para 4', 4),
(8, '4_1', 'para 4_1', 4),
(9, '5', 'para 5', 5),
(10, '5_1', 'para 5_1', 5),
(11, '6', 'para 6', 5),
(12, '6_1', 'para 6.1', 5),
(13, '7', 'p 7', 4),
(14, '7_1', 'p7.1', 4),
(15, '8', 'p 8', 3),
(16, '8_1', 'p 8.1', 3),
(17, '9', 'p 9', 2),
(18, '9_1', 'p 9.1', 2),
(19, '10', 'p 10', 1),
(20, '10_1', 'p 10.1', 1);
