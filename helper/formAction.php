<?php
# FORM HANDLER
add_action('admin_post_submit-hook', '_handle_hook_action');
function _handle_hook_action() {
    status_header(200);
    $datas = $_POST;
    $allowedArr = array(
        'hook_before_requirementBox',
        'hook_after_requirementHeading',
        'hook_after_requirementBox',
        'hook_after_benefitHeading',
        'hook_after_benefitBox'
    );
    echo '<br>';
    foreach ($datas as $key => $value) {
        if(in_array($key, $allowedArr)) {
            $var = $value || null;
            update_option($key, $value);
        }
    }
    // wp_redirect(get_admin_url().'admin.php?page=sedut-hook');
    // exit;
}

add_action('admin_post_submit-form', '_handle_form_action'); // If the user is logged in
// add_action('admin_post_nopriv_submit-form', '_handle_form_action'); // If the user in not logged in
function _handle_form_action()
{

    # debugger
    if (wp_get_current_user()->user_email === 'shahdanial.work@gmail.com') {
        sedutMaukerja();
        return;
    }

    status_header(200);
    $datas = $_POST;

    #clean data, remove unecesary items from obj..
    unset($datas['action']);
    unset($datas['hide']);
    foreach ($datas as $k => $perenggan) {
        // print_r($perenggan);
        try {
            global $wpdb;
            $wpdb->update(
                $wpdb->prefix . 'a1_perenggan_ratator',
                array('perenggan' => stripslashes($perenggan)),
                array( 'pNumber' => str_replace("para", "", $k) ),
                null,
                null
            );
        } catch (\Throwable $th) {
            $pluginlog = plugin_dir_path(__FILE__).'system.log';
            $message = $th.PHP_EOL;
            error_log($message, 3, $pluginlog);
        }
    }
    wp_redirect(get_admin_url().'admin.php?page=sedut-option');
    exit;
}
