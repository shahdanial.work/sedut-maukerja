<?php

/**
 * careful, please trim your $string
 */
function startsWith ($string, $startString)
{
    $len = strlen($startString);
	return (substr($string, 0, $len) === $startString);
}

/**
 * careful, please trim your $string
 */
function endsWith($string, $endString)
{
    $len = strlen($endString);
    if ($len == 0) {
        return true;
    }
    return (substr($string, -$len) === $endString);
}

function stringMatchInArray($string, $array){
    $count = count(array_intersect(array_map('strtolower', explode(' ', $string)), $array));
    if(0 < $count) {
        return $count;
    }
    return false;
}

function dynamicWpPostTitle($titleStr, $companyName) {
    if ( ! function_exists( 'post_exists' ) ) {
        require_once( ABSPATH . 'wp-admin/includes/post.php' );
    }
    # Jawatan kosong $namaJawatan
    // $seoTitle = post_exists('Jawatan kosong ' . $titleStr);
    // if($seoTitle == 0) return 'Jawatan kosong ' . $titleStr;
    # try direct with Title
    $directTitle = post_exists($titleStr);
    if($directTitle == 0) return $titleStr;
    # try by adding company name
    if($companyName){
        $arr = explode(' ', $companyName);
        if(!stringMatchInArray($titleStr, $arr)) {
            $withCompany = post_exists( $titleStr . ' di ' . $companyName );
            if($withCompany == 0) return $titleStr . ' di ' . $companyName;
        }
    }
    # try by adding  - num
    $titleStr = trim($titleStr);
    $toRet = false;
    $tambahan = '';
    $c = 0;
    while(!$toRet){
        $c++;
        $isExists = post_exists( $titleStr . $tambahan );
        if($isExists > 0) {
            $tambahan = ' - ' . $c;
        } else {
            # takde lg guna title ni..
            $toRet = $titleStr . $tambahan;
        }
    }
    return trim($toRet);
}

function formatPerenggan($perengganText) {
    $newLine = "\r\n\r\n";
    $text = trim($perengganText);
    # only allowed tag
    $text = strip_tags($text, '<p><ul><li><b><strong><i>');
    # remove inline style
    $text = preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $text);
    # remove all spaces after dots..
    $text = preg_replace("/\s*\.\s*/", ".", $text);
    # add new line after every dots.
    $text = str_replace('.', '.' . $newLine, $text);
    $text = preg_replace('/([,|!|\.])([[:alpha:]]{2,})/', '$1' .$newLine. '$2', $perengganText);
    # add space to lowerCaseToUppercase in 1 words.
    $pattern = '/(.*?[a-z]{1})([A-Z]{1}.*?)/';
    $replace = '${1}' .$newLine. '${2}';
    $text = preg_replace($pattern, $replace, $text);
    # remove all nbsp
    $html = str_replace('&nbsp;', '', $text);
    $emptyTag = "/<[^\/>]*>([\s]?)*<\/[^>]*>/";
    # remove all empty elements
    $html = preg_replace($emptyTag, '', $html);
    $html = preg_replace($emptyTag, '', $html);
    # replace all newline by adding 1 more lines
    $html = str_replace(PHP_EOL, PHP_EOL . PHP_EOL, $html);
    # set more than 2 new line to two newline
    $html = preg_replace( "/[\r\n\r\n]+\s+/", $newLine, $html);
    return $html;
}
/**
 * @param fileNamewithExtension my-pic.png
 * the return follow this: https://codex.wordpress.org/Function_Reference/wp_upload_bits
 */
function uploadImageByUrl($urlOfImage, $setPathName){
    $newExt = false;
    $readImgExt = array_reverse(explode('.', $urlOfImage));
    if(isset($readImgExt[0])){
        $cleanExt = explode('?', $readImgExt[0]);
        if(isset($cleanExt[0])){
            $allowedExt = array('png','gif','jpg', 'webp', 'jpeg');
            if(in_array(strtolower($cleanExt[0]), $allowedExt)){
                $newExt = strtolower($cleanExt[0]);
            }
        }
    }
    if(!$newExt) return false; // error, extension not allowed
    $tt = curl_get($urlOfImage);
    $newFileName = preg_replace('!\s+!', ' ', $setPathName);
    $newFileName = str_replace('-', ' ', $newFileName);
    $newFileName = strtolower($newFileName) . '.' . $newExt;
    $upload = wp_upload_bits($newFileName, null, $tt);
    // var_dump($upload);
    return $upload;
}

function isThisUniquePost($api, $sourceDomain){
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 1,
        'meta_query' => array(
            array(
                'key' => 'source_domain',
                'value' => $sourceDomain
            ),
            array(
                'key' => 'source_post_id',
                'value' => $api['job_id']
            )
        )
    );
    $the_query = get_posts( $args );
    # if already posted, return false, vise versa
    if($the_query) {
        // echo '<br>';
        // var_dump($the_query);

        // print_r('<br>id = '.$api['job_id']);
        // echo '<br>';
        return false;
    } else {
        return true;
    }
}

/**
 * if str, we will trim them first. then if str length 0 it will return false.
 * if obj we just direcly json_encode
 */
function strArrDbFormatter($var) {
    if(is_string($var)) {
        if(strlen(trim($var)) > 0){
            return trim($var);
        }
        return false;
    }
    if(is_array($var)) {
        return json_encode($var);
    }
}

function getContentHook(){
    $arr = array(
        'hook_before_requirementBox' => get_option('hook_before_requirementBox'),
        'hook_after_requirementHeading' => get_option('hook_after_requirementHeading'),
        'hook_after_requirementBox' => get_option('hook_after_requirementBox'),
        'hook_after_benefitHeading' => get_option('hook_after_benefitHeading'),
        'hook_after_benefitBox' => get_option('hook_after_benefitBox')
    );
    // TODO: get db setup here to overide default above..
    return $arr;
}

// this function not run on crawl. but on visit single post..
function replace_text($text) {
    $hookDb = getContentHook();
    foreach ($hookDb as $key => $value) {
        if($hookDb[$key]) {
            # show them ..
            $text = str_replace('<!-- '.$key.' -->', '<aside class="not-important style_'.$key.'">'.$value.'</aside>', $text);
        } else {
            # remove them ..
            $text = str_replace('<!-- '.$key.' -->', '', $text);
        }
    }
    $text = str_replace('<p></p>', '', $text);
	return $text;
}
add_filter('the_content', 'replace_text');