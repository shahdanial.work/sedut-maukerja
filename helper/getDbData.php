<?php
 function getDBrow()
 {
     global $wpdb;
     $rrow = $wpdb->get_results(
         "SELECT * FROM {$wpdb->prefix}a1_perenggan_ratator"
        );
     // print_r($rrow); return;
     // print_r($myrow);
     // return;
     // print_r('<pre>');
     // var_dump($row);
     // print_r('</pre><br><hr><br>');
     $myobj = array();
     for ($i=0; $i < count($rrow); $i++) {
         $obj = json_decode(json_encode($rrow[$i]), true);
         $myobj[$obj['pNumber']] = $obj;
         // print_r('<br>');
      // print_r(
      //  $obj
      //  //$rrow[$i]
      // );
      // print_r('<br>');
     }
     // print_r('<br>');
     //  print_r(array('aa'=> 111,'bb'=> 22));
     //  print_r('<br>');
     return $myobj;
 }

 function getLowRoator()
 {
     $rotators = getDBrow();
    //  if(is_array($rotators)) $rotators = shuffle($rotators);

     # 1. Find lowest used count
     $lowerGroupKey = '0';
     $lowerUsedCount = 9999999999999999;
     foreach ($rotators as $currentGroupKey => $obj) {
         if (strpos($currentGroupKey, '_') === false) { // takde `_` = perenggan first
             if (isset($obj['used'])) {
                 if ($obj['used'] < $lowerUsedCount) {
                     $lowerGroupKey = $currentGroupKey;
                     $lowerUsedCount = $obj['used'];
                 }
             }
         }
     }

     # group all para's child of lowerGroupKey..
     $toRet = array(
      0 => $rotators[$lowerGroupKey]
     );


     foreach ($rotators as $currentGroupKey => $obj) {
         if (!isset($toRet[$currentGroupKey]) && startsWith(trim($currentGroupKey), trim($lowerGroupKey) . '_')) {
             $toRet[1] = $obj;
         }
     }

     # then return
     return $toRet;
 }

function recordRotatorUsed($rotatorObj)
{
    foreach ($rotatorObj as  $obj) {
        if (is_array($obj)) {
            if (isset($obj['pNumber'])) {
                global $wpdb;
                $wpdb->query(
                    'UPDATE ' . $wpdb->prefix . 'a1_perenggan_ratator SET used = used+1 where pNumber = "'. $obj['pNumber'] .'"'
                );
            }
        } else {
            error_log('swasta.kerjakosong.co can\'t find $obj[\'pNumber\'] in recordRotatorUsed() on /helper/getDbData.php');
        }
    }
}
