<?php
# ADMIN PAGE
add_action('admin_menu', 'sedut_admin_page');
function sedut_admin_page()
{
    add_menu_page('Sedut configuration', 'Sedut', 'manage_options', 'sedut-option', 'controllerFunc');
    add_menu_page('Sedut Post Content Hook', 'Content Hook', 'manage_options', 'sedut-hook', 'sedutContentHook');
}

# Single Post Content Hook..
function sedutContentHook() {
    $arr = array(
        'hook_before_requirementBox' => get_option('hook_before_requirementBox'),
        'hook_after_requirementHeading' => get_option('hook_after_requirementHeading'),
        'hook_after_requirementBox' => get_option('hook_after_requirementBox'),
        'hook_after_benefitHeading' => get_option('hook_after_benefitHeading'),
        'hook_after_benefitBox' => get_option('hook_after_benefitBox')
    );

    echo '<style>
    .paraBoxHandle {
      margin: 1em 0 0;
    }
    .wp-editor-wrap {
        margin-bottom: 3em;
    }
    </style>';

    echo '<div class="wrap">';
    echo '<h1 class="wp-heading-inline">Entry content hooks</h1>';
    #------------ start  form ----------

    echo '<form action="'.get_admin_url().'admin-post.php" method="post">';

    # WP Form
    echo "<input type='hidden' name='action' value='submit-hook' />";

    foreach ($arr as $k => $v) {
        # heading ..
        echo '<h2 class="paraBoxHandle hndle ui-sortable-handle">' . str_replace('_', ' ', $k) .':</h2>';
        echo '<textarea name="'.$k.'" id="'.$k.'_input" class="large-text code" rows="3">'.$v.'</textarea>';
    }

    echo '<br/><br/><input type="submit" id="submit" class="button button-primary" value="Save Changes">';

    echo '</form>';

    #----------- end form -------------
    echo '</div>';
}

# PAGE CONTENT
function controllerFunc()
{
    $paraRow = getDBrow();
    // print_r('<pre>');
    // var_dump($paraRow);
    // print_r('</pre><br><hr><br>');
    // print_r('<hr><br>');
    #----------------------- CSS --------------------------
    echo '<style>
    .paraBoxHandle {
      margin: 1em 0 -1.6em;
    }
    .wp-editor-wrap {
        margin-bottom: 3em;
    }
    </style>';

    #----------------------- HTML --------------------------
    echo '<div class="wrap">';
    echo '<h1 class="wp-heading-inline">Perenggan Rotator</h1>';
    #------------ start  form ----------

    echo '<div class="notice notice-success">Notes: anda boleh menggunakan <code>$title</code> sebagai ayat dynamic yang mana ianya akan automatically convert kepada post title post tersebut. It\'s naturally good to have it on every rorator since it\'s dynamic!</div>';

    echo '<form action="'.get_admin_url().'admin-post.php" method="post">';

    # WP Form
    echo "<input type='hidden' name='action' value='submit-form' />";
    // echo "<input type='hidden' name='hide' value='$ques' />";

    foreach ($paraRow as $para) {
        echo '<h2 class="paraBoxHandle hndle ui-sortable-handle">Peranggan '. str_replace("_", ".", $para['pNumber']) .'</h2>';
        $setup1 = array( 'media_buttons' => false );
        wp_editor(
            $para['perenggan'],
            'para'. $para['pNumber'],
            array( // setup
                    'textarea_rows' => 2,
                    'media_buttons' => false
                )
            );
    }

    echo '<input type="submit" id="submit" class="button button-primary" value="Save Changes">';

    echo '</form>';

    #-------------- end form -----------
    echo '</div>';
}
